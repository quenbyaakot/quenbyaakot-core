use std::{net::{SocketAddr, IpAddr}, fmt::Debug};
use serde::{Deserialize, Serialize};

/// Store some info about target machine.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct TargetMachine {
    /// Public machine address.
    pub address: IpAddr,
    /// Root user password.
    pub password: String,
    /// Machine's name.
    pub name: String,
    /// Machine's description.
    pub description: String,
    /// Path to the machine's icon on the web server.
    pub icon: String,
    /// Points that red team receive's when machine hacked.
    pub points: i16,
}

/// Represent team's role. Attacker - Red, Defender - Blue.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq)]
pub enum Role {
    /// Red team.
    Attacker,
    /// Blue team.
    Defender,
}

/// Represents challenge's team.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Team {
    /// Team's name or alias.
    pub name: String,
    /// Team's role.
    pub role: Role,
    /// Team's password.
    pub password: String,
}

/// Challenge config object.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct ChallengeConfig {
    /// Listen address of the websocket server.
    pub ws_addr: SocketAddr,
    /// Listen address of the health-moniotr service.
    pub tcp_addr: SocketAddr,
    /// List of available machines.
    pub machines: Vec<TargetMachine>,
    /// Maximal revival time in milliseconds.
    pub max_revival_time: u64,
    /// Red and blue teams.
    pub teams: [Team; 2],
}


/// Represents a machine state.
#[derive(Deserialize, Serialize, Debug, Copy, Clone)]
pub enum State {
    /// The machine was never connected to the server.
    Unavailable,
    /// The machine disconnected from the server. First value is time when the machine was disconnected.
    Disconnected(u128),
    /// The machine currently connected to the server. First value is time when the machine was (re)-connected.
    Connected(u128),
    /// The machine has been hacked. First value is time when the machine was hacked.
    Hacked(u128),
}